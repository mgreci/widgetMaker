const mongodb = require('mongodb');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const PORT = 4000;

const MongoClient = mongodb.MongoClient;
const mongoURL = "mongodb://ec2-18-236-148-165.us-west-2.compute.amazonaws.com:28017";
let database;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const allWidgets = () => {
  database.find({}).toArray().then((docs) => {
    widgets = docs;
    io.emit('widgets', widgets);
  }).catch((err) => {
    console.error("Error querying widgets: " + err);
    widgets = [];
    io.emit('widgets', widgets);
  });
};

app.post('/widget', (req, res) => {
  let response = {
    success: false,
    message: ""
  };

  if (req.body) {
    let widget = req.body;
    database.insertOne(widget).then((result) => {
      response.success = true;
      response.message = `Saved to database`;
      allWidgets();
      res.send(response);
    }).catch((err) => {
      response.message = `Error inserting:  ${err}`;
      console.error(response.message);
      res.status(500).send(response);
    });
  } else {
    res.status(400);
  }
});

app.delete('/widget/:id', (req, res) => {
  let response = {
    success: false,
    message: ""
  };

  const id = req.params.id;

  if (!id || typeof id !== "string" || id.length !== 24) {
    res.status(400).send(`Invalid widget ID '${id}'`);
    return;
  }

  database.deleteOne({_id: new mongodb.ObjectID(id)}).then((result) => {
    response.success = true;
    response.message = `delete widget ${id}`;
    console.log(response.message);
    allWidgets();
    res.send(response);
  }).catch((err) => {
    response.message = `error deleting widget ${id}: ${err}`;
    console.log(response.message);
    res.status(500).send(response);
  });

});

let widgets = [];

io.on('connection', (socket) => {
  io.emit('widgets', widgets);
});

http.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});

MongoClient.connect(mongoURL).then((db) => {
  console.log("DB connected");
  database = db.collection('widgets');
  allWidgets();
}).catch((err) => {
  console.error("Error connecting to mongoDB: " + err);
  process.exit();
});
