FROM node:slim
RUN npm install --global nodemon
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD . ./
EXPOSE 4000
CMD npm start
