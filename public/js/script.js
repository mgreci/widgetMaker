const widgets = document.getElementById("widgets");
const widgetListBody = widgets.querySelector(".card-body");
const list = widgets.querySelector("#list");
const count = widgets.querySelector("#count");
const tbody = document.querySelector("table#createWidget tbody");

const socket = io();

const updateWidgets = (widgets) => {
  //update widget count
  count.innerHTML = widgets.length;

  //remove all HTML DOM nodes
  while (list.firstChild) {
    list.removeChild(list.firstChild);
  }

  //display widget list (if any came back)
  if (widgets.length > 0) {
    //display widget card-body
    widgetListBody.classList.remove('d-none');
    //for each widget, display its data
    widgets.forEach((widget) => {
      let deleteButton = document.createElement('button');
      deleteButton.classList.add('btn', 'btn-sm', 'btn-danger', 'float-right');
      deleteButton.id = widget._id;
      deleteButton.innerHTML = "Delete";
      deleteButton.addEventListener('click', () => {
        deleteWidget(deleteButton.id);
      });

      let li = document.createElement("li");
      li.classList.add('list-group-item');

      let pre = document.createElement("pre");
      pre.innerHTML = JSON.stringify(widget, null, 2);

      li.appendChild(deleteButton);
      li.appendChild(pre);

      list.appendChild(li);
    });
  } else {
    widgetListBody.classList.add('d-none');
  }
};

socket.on('widgets', (widgets) => {
  updateWidgets(widgets);
});

const createWidget = () => {
  //generate widget (from user given data)
  let widget = {
    "createdAt": new Date().toISOString()
  };

  let propertyRows = document.querySelectorAll("tbody tr");
  let property, value;
  propertyRows.forEach((row) => {
    property = row.querySelector("input.property").value;
    value = row.querySelector("input.value").value;
    if (property !== "" && row !== "") {
      //property and value are good, add to widget
      widget[property] = value;
    }
  });

  //stringify for transport over HTTP
  widget = JSON.stringify(widget);

  //create HTTP request and send
  const createWidgetURL = "/widget";
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: widget
  };

  fetch(createWidgetURL, options)
    .then(response => response.json())
    .then((result) => {
    if (result.success) {
      //modal popup with result
      $("#successfulCreation").modal('toggle');
    } else {
      console.log("Did not create widget: " + result.message);
    }
  }).catch((error) => {
    console.log(error);
  });
};

const deleteWidget = (id) => {
  if (!id || typeof id !== "string" || id.length !== 24) {
    console.warn(`Invalid widget ID '${id}'`);
    return;
  }

  //create HTTP request and send
  let deleteWidgetURL = `/widget/${id}`;
  let options = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  fetch(deleteWidgetURL, options)
    .then(response => response.json())
    .then((result) => {
      if (result.success) {
        //modal popup with result
        $("#successfulDeletion #id").text(id);
        $("#successfulDeletion").modal('toggle');
      } else {
        console.log(`Unable to delete widget: ${id}`);
      }
    }).catch((error) => {
      console.error(error);
    });
}

const addPropertyRow = () => {
  //copy first table row
  let row = document.getElementById("first").cloneNode(true);
  //append to tbody
  tbody.appendChild(row);
  //clear new row
  row.querySelector(".property").value = "";
  row.querySelector(".value").value = "";
};
