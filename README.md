# Widget Maker
*Created by Matthew Greci*

Live @ http://ec2-18-236-148-165.us-west-2.compute.amazonaws.com

## Description
Simple Node/Express/MongoDB application to demo full stack web development.

This project allows a user to create, view, and delete widgets from the frontend.

## How it Works
A single [AWS EC2](https://aws.amazon.com/ec2/) instance hosts a [Docker](https://www.docker.com) engine which allows both a [Node.js](https://nodejs.org/) server as well as [MongoDB](https://www.mongodb.com) to run simultaneously.

### Frontend
The same Node server also serves the HTML frontend which utilizes [Bootstrap](https://getbootstrap.com).

### Backend
A single Node server uses the module [Express](https://expressjs.com) to handle the HTTP API routing (GET, POST, and DELETE).

### Database
MongoDB is running on the same AWS EC2 instance, via Docker. In my case, this is done to save on running another EC2 instance but would typically run in its own instance or possibly replaced with [AWS DocumentDB](https://aws.amazon.com/documentdb/) (or similar).

# What's Missing?
Most applications utilizing Node/Express/MongoDB also use [Angular](https://angular.io). I found that Angular would be too much for this project, but could be added as the demo project became more complicated.
