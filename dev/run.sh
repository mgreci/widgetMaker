#!/bin/bash

IMAGE="mgreci/widgetmaker"
CONTAINER="app"
PORT="5000"

docker run -d -p $PORT:4000 --name $CONTAINER --volume $PWD:/app $IMAGE nodemon app.js
