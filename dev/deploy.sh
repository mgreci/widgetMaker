#!/bin/bash

IMAGE="mgreci/widgetmaker"

# Build the image
docker image build -t $IMAGE .\
  && docker push $IMAGE\
  && ssh ec2-user@ec2-18-236-148-165.us-west-2.compute.amazonaws.com bash ./deploy.sh
